#!/usr/bin/env python3

import sys,os
import re, csv
import requests
from datetime import date, datetime
from collections import namedtuple
import json, toml
import os.path
from canvasapi import Canvas

CanvasStudent = namedtuple('CanvasStudent', 'name, canvasid, cruzid')
RubricItem = namedtuple ('RubricItem', 'description, points, comments', defaults=(None, None, None))

class CanvasCourse:
    def __init__ (self, config_file):
        self.config_file = config_file
        with open(self.config_file) as f:
            self.config = toml.load (f)
        self.canvas = Canvas (self['canvas_url'], self['canvas_token'])
        self.course = self.canvas.get_course (self['canvas_course_id'])
        self.assignments = {x.name: CanvasAssignment (x, self.config) for x in self.course.get_assignments()}
        self.repo_base = os.path.join (self['gitlab_url'], self['gitlab_term_base'])
        self.students = dict()
        for u in self.course.get_users ():
            name = u.sortable_name
            canvasid = u.id
            email = u.login_id
            cruzid = re.search(r'(.*?)(\+.*?)?@ucsc.edu', email).group(1)
            self.students[cruzid] = (CanvasStudent(name, canvasid, cruzid))

    def __getitem__ (self, k):
        return self.config[k]

    def get_assignment_by_name (self, nm):
        for a in self.assignments.keys():
            if re.search (nm, a):
                return self.assignments[a]
        raise ValueError ('No assignment matches {0}'.format (nm))

    def get_roster (self, roles=['student'], states=['active']):
        students = list(self.students.values())
        return students

    def get_student_canvas_id (self, cruzid):
        return self.students[cruzid].canvasid

    def write_roster_as_csv (self, filedesc, delimiter=','):
        '''
        Write the roster to a CSV file.

        filedesc:  an open (writeable) file descriptor
        delimiter: the delimiter to use (default is ',', but '\t' also makes sense)
        '''
        writer = csv.DictWriter(filedesc, fieldnames=('name', 'canvas_id', 'CruzID', 'repo'), delimiter=delimiter)
        writer.writeheader ()
        for stu in self.get_roster ():
            writer.writerow ({'name': stu.name, 'canvas_id': stu.canvasid, 'CruzID': stu.cruzid,
                              'repo': os.path.join (self.repo_base, stu.cruzid)})


class CanvasAssignment:
    def __init__ (self, asgn, config):
        self.assignment = asgn
        try:
            self.rubric = self.assignment.rubric
            self.config = config
            self.rubric_fields_desc = {x['description'] : x['id'] for x in self.rubric}
            self.rubric_fields_short = {x.lower()[:4] : self.rubric_fields_desc[x] for x in self.rubric_fields_desc.keys()}
            if len(self.rubric_fields_desc) != len(self.rubric_fields_short):
                raise ValueError('First four letters of each rubric description must be unique')
            self.rubric_create_url = os.path.join (config['canvas_url'],
                                                   'api/v1/courses',
                                                   str(self.config['canvas_course_id']),
                                                   'assignments',
                                                   str(self.assignment.id),
                                                   'submissions')
        except:
            self.rubric = None

    def get_submission (self, student_id):
        submission = self.assignment.get_submission (student_id, include=['rubric_assessment'])
        return submission

    def update_submission (self, student_id, updated_assessments):
        '''
        Update a submission rubric based on the list of updated assessments passed in.
        The numeric student_id can be obtained by calling CanvasCourse.get_roster().

        student_id: numeric ID of the student
        updated_assessments: list of updated assessments, each of which is a RubricItem
                             Passing None for points or comments leaves them unchanged.

        If the description of a rubric item doesn't match an item in the actual rubric,
        a KeyError is raised. The description only needs to match the first four characters
        of the actual description in the rubric, and is case-insensitive.
        '''
        submission = self.get_submission (student_id)
        try:
            a = submission.rubric_assessment
        except:
            a = dict()
            submission.rubric_assessment = a
        for k in self.rubric_fields_desc.values():
            if k not in a:
                a[k] = dict()
                a[k]['comments'] = ' '
        if type(updated_assessments) == type(RubricItem()):
            updates = [updated_assessments]
        else:
            updates = updated_assessments
        for x in updates:
            rk = x.description.lower()[:4]
            if rk in self.rubric_fields_short:
                rubric_key = self.rubric_fields_short[rk]
            elif rk in a:
                rubric_key = rk
            else:
                raise KeyError ('No rubric item matching {0}'.format (description))
            itm = a.get(rubric_key, dict())
            if x.comments != None:
                itm['comments'] = x.comments
            if x.points != None:
                itm['points'] = x.points
            submission.edit (rubric_assessment=submission.rubric_assessment)

    def get_rubric (self):
        return dict(self.rubric)

    def write_submission_as_json (self, student_id, filedesc):
        sbm = self.get_submission (student_id)
        json.dump (vars(sbm), filedesc, default=json_serialize, indent=2,sort_keys=True)
        f.write ('\n\n')

def json_serialize(obj):
    if isinstance (obj, (datetime, date)):
        return obj.isoformat ()
    return str(obj)

def upload_scores (course, d):
    '''
    Upload scores and comments to an assignment rubric.
    '''
    for asgn in d.keys():
        assignment = course.get_assignment_by_name(d[asgn]['name'])
        rubric_field = d[asgn]['rubric_field']
        for student, x in d[asgn]['scores'].items():
            itm = RubricItem(rubric_field, x['points'], x['comments'])
            canvasid = course.get_student_canvas_id(student)
            print ("{4}: Updating {0} ({3}) to points={1}, comments={2}".format (student, itm.points, itm.comments[-10:], canvasid, itm.description))
            assignment.update_submission(canvasid, [itm])

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser (prog = sys.argv[0],
                                      description='Program to interact with the Canvas API')
    parser.add_argument ('operation',
                         choices=('roster', 'tabroster',
                                  'pushfiles', 'rmfiles',
                                  'dumpjson', 'uploadscores',
                                  'createrepos', 'clone', 'checkout', 'addtorepos'),
                         help='Operation to perform')
    parser.add_argument ('-c', action='store', default='canvasapi.toml',
                         dest='config_file',
                         help='Configuration file to use')
    parser.add_argument ('-a', action='store', default=None, dest='assignment',
                         help='Assignment to which grades will be uploaded')
    args = parser.parse_args()

    course = CanvasCourse (args.config_file)

    if args.operation == 'tabroster':
        course.write_roster_as_csv(sys.stdout, '\t')
    elif args.operation == 'roster':
        course.write_roster_as_csv(sys.stdout)
    elif args.operation == 'uploadscores':
        scores = json.load (sys.stdin)
        upload_scores(course, scores)
