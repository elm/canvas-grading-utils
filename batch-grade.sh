#!/usr/bin/env bash


export REPO_BASE=git@git.ucsc.edu:cse13s/fall22
export TESTDIR=/home/elm/cse13s/testing
export RUNDIR=`pwd`
export ASSIGNMENT=asgn.3
export JSON_FILE=$TESTDIR/scores-asgn3.json

declare -A commits

while read commitid user; do
    commits[$user]=$commitid
done < $TESTDIR/asgn3-commitids.txt

while IFS=$'\t' read name canvasid user; do
    cd $RUNDIR
    echo -n "=========="; date
    echo "Running tests for $user"
    if [ -z "${commits[$user]}" ]; then
        echo "$user does not have a commit ID - skipping!"
        $TESTDIR/jsoncomment.py $JSON_FILE $ASSIGNMENT $user 0 "No commit ID submitted on the Google form."
        continue
    fi
    if [ ! -d $user ]; then
        git clone $REPO_BASE/$user.git
    fi
    cd $user
    git fetch --all
    git checkout -f ${commits[$user]}
    python3 $TESTDIR/check_submission.py $TESTDIR/cse13s-asgn3.toml check >> $TESTDIR/checks.out
    if [ $? -ne 0 ]; then
        echo "$user failed build check - skipping functional test!"
        $TESTDIR/jsoncomment.py $JSON_FILE $ASSIGNMENT $user 0 "Commit ID ${commits[$user]} failed minimal requirements check. Maximum assignment grade is 5 points."
        continue
    fi
    date ; echo -n "Running grading for $user ... "
    python3 $TESTDIR/check_submission.py $TESTDIR/cse13s-asgn3.toml test grade >> $TESTDIR/tests.out
    echo -n "done at "; date
done
